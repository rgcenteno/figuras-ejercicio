/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cs1daw.figuras;

import org.daw1.figuras.*;
/**
 *
 * @author rgcenteno
 */
public class Figuras {

    static java.util.Scanner teclado = new java.util.Scanner(System.in);
    
    private Figuras(){
        
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*crearCirculos();
        crearRectangulos();
        crearCuadrados();
        crearTriangulos();
        distanciaPunto();
        mix();*/
        figurasProgram();
    }
    
    public static void crearCirculos(){             
        //Primera forma
        Circunferencia c1 = new Circunferencia(2);
        Circunferencia c2 = new Circunferencia(3);
        Circunferencia c3 = new Circunferencia(4);
        Circunferencia c4 = new Circunferencia(5);  
        
        System.out.printf("Circunferencia c1 Área: %f. Perímetro %f\n", c1.getArea(), c1.getPerimetro());
        System.out.printf("Circunferencia c2 Área: %f. Perímetro %f\n", c2.getArea(), c2.getPerimetro());
        System.out.printf("Circunferencia c3 Área: %f. Perímetro %f\n", c3.getArea(), c3.getPerimetro());
        System.out.printf("Circunferencia c4 Área: %f. Perímetro %f\n", c4.getArea(), c4.getPerimetro());
        
        //Otra forma de crearlos en una array. Ambas son válidas
        Circunferencia[] arrayCircunferencia = new Circunferencia[4];
        for(int i = 2; i<= 5; i++){
            arrayCircunferencia[i - 2] = new Circunferencia(i);
        }
        for(Circunferencia c : arrayCircunferencia){
            System.out.printf("Circunferencia c Área: %f. Perímetro %f\n", c.getArea(), c.getPerimetro());
        }
    }
    
    public static void crearRectangulos(){
        Rectangulo[] arrayRect = new Rectangulo[4];
        arrayRect[0] = new Rectangulo(8,4);
        arrayRect[1] = new Rectangulo(3,5);
        arrayRect[2] = new Rectangulo(6,7);
        arrayRect[3] = new Rectangulo(10, 20);
        for (int i = 0; i < arrayRect.length; i++) {
            Rectangulo rectangulo = arrayRect[i];
            System.out.printf("Rectangulo r%d. Área: %f. Perímetro: %f\n", (i+1), rectangulo.getArea(), rectangulo.getPerimetro());            
        }
    }   

    public static void crearCuadrados(){
        Cuadrado[] arrayCuad = new Cuadrado[5];
        //Si se hace a mano, no está mal
        for (int i = 0; i < arrayCuad.length; i++) {
            arrayCuad[i] = new Cuadrado( 4 + (i * 2));
        }
        for(Cuadrado c : arrayCuad){
            System.out.printf("Cuadrado. Área: %f. Perímetro: %f\n", c.getArea(), c.getPerimetro());            
        }
    }
    
    public static void crearTriangulos(){
        TrianguloRectangulo[] arrayT = new TrianguloRectangulo[4];
        arrayT[0] = new TrianguloRectangulo(8, 4);
        arrayT[1] = new TrianguloRectangulo(3, 5);
        arrayT[2] = new TrianguloRectangulo(6, 7);
        arrayT[3] = new TrianguloRectangulo(10, 20);
        for (int i = 0; i < arrayT.length; i++) {
            TrianguloRectangulo t = arrayT[i];
            System.out.printf("Triangulo %d. Área: %f. Perímetro: %f \n", (i+1), t.getArea(), t.getPerimetro());
        }
    }
    
    public static void distanciaPunto(){
        Punto p1 = new Punto(-1, 3);
        Punto p2 = new Punto(2, 2);
        System.out.println("Distancia: " + p1.getDistancia(p2));
    }
    
    public static void mix(){
        Figura[] figuras = new Figura[3];
        figuras[0] = new Rectangulo(8, 10);
        figuras[1] = new TrianguloRectangulo(5, 9);
        figuras[2] = new Circunferencia(5);
        for (int i = 0; i < figuras.length; i++) {            
            System.out.printf("Figura %s. Área: %f. Perímetro: %f\n", figuras[i].getClass().getSimpleName(), figuras[i].getArea(), figuras[i].getPerimetro());            
        }
    }
    
    public static void figurasProgram(){
        
        String opcion = "";
        do{
            System.out.println("*******************************************");
            System.out.println("* 1. Circunferencia                       *");
            System.out.println("* 2. Triángulo Rectángulo                 *");
            System.out.println("* 3. Cuadrado                             *");
            System.out.println("* 4. Rectángulo                           *");            
            System.out.println("* 5. Distancia entre dos puntos           *");
            System.out.println("*                                         *");
            System.out.println("* 0. Salir                                *");
            System.out.println("*******************************************");
            opcion = teclado.nextLine();
        
            switch(opcion){
                case "1":
                    programCircunferencia();
                    break;
                case "2":
                    programTriangulo();
                    break;
                case "3":
                    programCuadrado();
                    break;
                case "4":
                    programRectangulo();
                    break;
                case "5":
                    distanciasPuntosProgram();
                    break;
                case "0":
                    break;
                default:
                    System.out.println("Opción inválida");

            }
        }while(!opcion.equalsIgnoreCase("0"));
    }
    
    public static void programCircunferencia(){        
        int radio = -1;
        do{
            System.out.println("Por favor inserte un radio para la circunferencia");
            if(teclado.hasNextInt()){
                radio = teclado.nextInt();
                if(radio > 0){
                    Circunferencia c1 = new Circunferencia(radio);
                    System.out.printf("Aŕea del círculo: %f. Perímetro: %f\n", c1.getArea(), c1.getPerimetro());
                }
                else{
                    System.out.println("El radio debe ser mayor que cero");
                }
            }
            else{
                System.out.println("Inserte un número entero");                
            }
            teclado.nextLine();
        }while(radio <= 0);
    }
    
    public static void programCuadrado(){        
        int lado = -1;
        do{
            System.out.println("Por favor inserte un lado para el cuadrado");
            if(teclado.hasNextInt()){
                lado = teclado.nextInt();
                if(lado > 0){
                    Cuadrado cuadrado = new Cuadrado(lado);
                    System.out.printf("Aŕea del cuadrado: %f. Perímetro: %f\n", cuadrado.getArea(), cuadrado.getPerimetro());
                }
                else{
                    System.out.println("El lado debe ser mayor que cero");
                }
            }
            else{
                System.out.println("Inserte un número entero");                
            }
            teclado.nextLine();
        }while(lado <= 0);
    }
    
    public static void programTriangulo(){        
        int base = -1;
        do{
            System.out.println("Por favor inserte una base para el triángulo");
            if(teclado.hasNextInt()){
                base = teclado.nextInt();
                if(base <= 0){
                    System.out.println("La base debe ser mayor que cero");
                }
            }
            else{
                System.out.println("Inserte un número entero");                
            }
            teclado.nextLine();
        }while(base <= 0);
        
        int altura = -1;
        do{
            System.out.println("Por favor inserte una altura para el triángulo");
            if(teclado.hasNextInt()){
                altura = teclado.nextInt();
                if(altura <= 0){
                    System.out.println("La altura debe ser mayor que cero");
                }
            }
            else{
                System.out.println("Inserte un número entero");                
            }
            teclado.nextLine();
        }while(altura <= 0);
        
        TrianguloRectangulo t1 = new TrianguloRectangulo(base, altura);
        System.out.printf("Triángulo rectángulo. Área: %f. Perímetro: %f", t1.getArea(), t1.getPerimetro());
    }
    
    public static void programRectangulo(){        
        int base = -1;
        do{
            System.out.println("Por favor inserte una base para el rectángulo");
            if(teclado.hasNextInt()){
                base = teclado.nextInt();
                if(base <= 0){
                    System.out.println("La base debe ser mayor que cero");
                }
            }
            else{
                System.out.println("Inserte un número entero");                
            }
            teclado.nextLine();
        }while(base <= 0);
        
        int altura = -1;
        do{
            System.out.println("Por favor inserte una altura para el rectángulo");
            if(teclado.hasNextInt()){
                altura = teclado.nextInt();
                if(altura <= 0){
                    System.out.println("La altura debe ser mayor que cero");
                }
            }
            else{
                System.out.println("Inserte un número entero");                
            }
            teclado.nextLine();
        }while(altura <= 0);
        
        Rectangulo r1 = new Rectangulo(base, altura);
        System.out.printf("Rectángulo. Área: %f. Perímetro: %f", r1.getArea(), r1.getPerimetro());
    }
    
    public static void distanciasPuntosProgram(){
        System.out.println("Punto 1");
        Punto p1 = crearPunto();
        System.out.println("Punto 2");
        Punto p2 = crearPunto();
        System.out.printf("Distancia entre p1 y p2 %f\n", p1.getDistancia(p2));
    }
    
    public static Punto crearPunto(){
        int ejeX = -1;
        boolean valido = false;
        do{
            System.out.println("Por favor inserte el valor en el eje x");
            if(teclado.hasNextInt()){
                ejeX = teclado.nextInt(); 
                valido = true;
            }
            else{
                System.out.println("Inserte un número entero");                
            }
            teclado.nextLine();
        }while(!valido);
        
        int ejeY = -1;
        valido = false;
        do{
            System.out.println("Por favor inserte el valor en el eje y");
            if(teclado.hasNextInt()){
                ejeY = teclado.nextInt(); 
                valido = true;
            }
            else{
                System.out.println("Inserte un número entero");                
            }
            teclado.nextLine();
        }while(!valido);        
        
        return new Punto(ejeX, ejeY);
    }
    
}
